# STPA Control Model for Autonomous Vehicles

All work here is Work In Progress, and is published on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

This repo contains a Work In Progress (WIP) Systems Theoretic Process Analysis (STPA) for autonomous vehicles. The aim is to develop a base set of safety requirements which will be widely applicable to autonomous vehicles.

> STPA (System-Theoretic Process Analysis) is a relatively new hazard analysis technique based on an extended model of accident causation. In addition to component failures, STPA assumes that accidents can also be caused by unsafe interactions of system components, none of which may have failed.

The above is from the STPA handbook, which can be found [here](http://psas.scripts.mit.edu/home/materials/).

## Apollo 2.0

STPA has been performed for Apollo version 2.0, and the results can be found in [apollo-example](apollo-example) directory.
