# WIP: Finding out about Apollo

All work here is Work In Progress, and is published on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

Here we are aiming to investigate the trustability (including safety) of
the [Apollo project](https://github.com/ApolloAuto/apollo) which is
made available by Baidu under the Apache 2.0 License.

Considering safety specifically, we'll be attempting to apply STPA, on
Apollo release v2.0.0. The **code-analysis** directory contains analysis of the Apollo 2.0 source code, while **STPA** contains the results from the safety analysis.
