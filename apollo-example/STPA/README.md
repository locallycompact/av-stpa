# WIP : STPA

All work here is Work In Progress, and is published on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

STPA is split into 4 steps:

* Define the purpose of the analysis
  - [Losses and Hazards](losses-and-hazards.md)
* Model the control structure
  - [Control Diagram](level-1/level1DotDiagram/level1-control-diagram.png)
* Identify unsafe control actions
  - [Unsafe Control Actions](level-1/stpa.md)
* Identify loss scenarios
  - [Scenario Analysis](level-1/Level1Type1and2Scenarios.md)
