# Losses

* L-1 : Loss of life or injury to people
* L-2 : Loss of or damage to vehicle
* L-3 : Loss of or damage to objects outside the vehicle
* L-4 : Loss of transportation mission
* L-5 : Loss of traffic flow (road blockages etc.)
* L-6 : Loss of customer satisfaction
* L-7 : Environmental impact

# Hazards

* H-1 : Vehicle does not maintain safe distance from terrain and other obstacles [L-1, L-2, L-3, L-4, L-5, L-6]
* H-2 : Vehicle drives too fast [L-1, L-2, L-3, L-4, L-5, L-6, L-7]
* H-3 : Excessive braking [L-1, L-2, L-3, L-4, L-5, L-6, L-7]
* H-4 : Vehicle does not follow traffic flow e.g. jumps red lights, drives on wrong side of the road [L-1, L-2, L-3, L-4, L-5, L-6, L-7]
* H-5 : Vehicle is unpredictable to others e.g. no indicators, drives on wrong side of road [L-1, L-2, L-3, L-4, L-5, L-6]
